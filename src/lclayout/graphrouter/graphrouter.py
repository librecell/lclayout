# Copyright 2019-2020 Thomas Kramer.
# SPDX-FileCopyrightText: 2022 Thomas Kramer
#
# SPDX-License-Identifier: CERN-OHL-S-2.0

import networkx as nx

from typing import Any, Dict, List, AbstractSet, Optional, Iterable

class GraphRoutingProblem:
    """
    Representation of a multi-signal routing problem in a graph.
    """

    def __init__(self, 
              graph: nx.Graph,
              signals: Dict[Any, List[Any]],
              reserved_nodes: Optional[Dict[Any, AbstractSet[Any]]] = None,
              node_conflict: Optional[Dict[Any, AbstractSet[Any]]] = None,
              equivalent_nodes: Optional[Dict[Any, AbstractSet[Any]]] = None,
              is_virtual_node_fn=None
                 ):
        """

        :param graph: Routing graph.
        :param signals: Mapping of signal names to terminal nodes in the graph.
        :param reserved_nodes: Mapping of signal names to graph nodes that are reserved for this signal.
        :param node_conflict: Mapping of a node to other nodes that can not be used for routing at the same time.
        :param equivalent_nodes: For each node a set of nodes that are physically equivalent.
        :param is_virtual_node_fn: Function that returns True iff the argument is a virtual node.
        :return: Returns a dict mapping signal names to routing trees.
        """
        
        self.graph: nx.Graph = graph
        self.signals: Dict[Any, List[Any]] = signals
        self.reserved_nodes: Optional[Dict[Any, AbstractSet[Any]]] = reserved_nodes
        self.node_conflict: Optional[Dict[Any, AbstractSet[Any]]] = node_conflict
        self.equivalent_nodes: Optional[Dict[Any, AbstractSet[Any]]] = equivalent_nodes
        self.is_virtual_node_fn=is_virtual_node_fn

        if self.node_conflict is None:
            self.node_conflict = dict()
        if self.equivalent_nodes is None:
            self.equivalent_nodes = dict()
        if self.reserved_nodes is None:
            self.reserved_nodes = dict()


class GraphRouter:

    def route(self, routing_problem: GraphRoutingProblem) -> Iterable[Dict[Any, nx.Graph]]:
        pass
