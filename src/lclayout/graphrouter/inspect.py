# SPDX-FileCopyrightText: 2024 Thomas Kramer
# SPDX-License-Identifier: CERN-OHL-S-2.0

"""
Wrap a graph router and add visual inspection capabilities.
"""

import os
import math
import networkx as nx
from ..layout.layers import layermap

from typing import *
import logging
from .graphrouter import GraphRouter, GraphRoutingProblem

import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)

class InspectRouter(GraphRouter):
    """
    Wrapps around a GraphRouter and adds visual inspection.
    """

    def __init__(self,
                 sub_graphrouter: GraphRouter,
                 plot_to_files = True,
                 output_dir: str = "/tmp"):
        self.sub_graphrouter = sub_graphrouter
        self.output_dir = output_dir
        self.plot_to_files = plot_to_files
        self.iteration_count = 0
        self._graph = None
        self._is_virtual_node_fn = None

    def route(self, routing_problem: GraphRoutingProblem) -> Iterable[Dict[Any, nx.Graph]]:

        self._graph = routing_problem.graph
        self._is_virtual_node_fn = routing_problem.is_virtual_node_fn

        solutions = self.sub_graphrouter.route(routing_problem)

        for routes in solutions:
            if routes:
                self._inspect(routes)
            yield routes
            self.iteration_count += 1


    def _inspect(self, routes: Dict[Any, nx.Graph]):
        if self.plot_to_files:
            self._plot_to_file(routes)
        
    def _plot_to_file(self, routes: Dict[Any, nx.Graph]):
        path = os.path.join(self.output_dir, f"routing_iter_{self.iteration_count:04}.png")

        # Compute offsets for pseudo 3D plotting.
        layer_names = sorted(layermap.keys(), key=lambda n: layermap[n][0])
        layer_offset = {n: i for i, n in enumerate(layer_names)}
        
        # Find positions of graph nodes.
        pos = dict()
        for n in self._graph.nodes():
            if not self._is_virtual_node_fn(n):
                layer, (x, y) = n
                offset = 16*layer_offset[layer]
                pos[n] = (x-offset, y+offset)
                
        def hue2rgb(hue: float) -> str:
            # Create a HTML color code from a hue value.
            assert 0. <= hue <= 1.

            a = 2 * math.pi / 3
            r,g,b = (int(255* math.cos(hue*math.pi + i*a) ** 2) for i in range(3))

            return f"#{r:02x}{g:02x}{b:02x}"
                
        net_names = sorted(routes.keys())
        num_routes = len(routes)
        colors = (hue2rgb(i/(num_routes+1)) for i in range(num_routes))
        
        for name, color in zip(net_names, colors):
            tree = routes[name]
            edges = tree.edges
            edges = [e for e in edges if not self._is_virtual_node_fn(e[0]) and not self._is_virtual_node_fn(e[1])]
            nx.draw_networkx_edges(self._graph, pos, edgelist=edges, width=4, edge_color=color)

        #nx.draw_networkx_nodes(self._graph, pos, nodelist=list(chain(*signals.values())), node_size=32, node_color='black')
        #nx.draw_networkx_edge_labels(self._graph, pos, edge_labels=edge_labels)

        plt.draw()
        logger.debug(f"Plot routes: {path}")
        plt.savefig(path)
        plt.close()

